import logging

from ariadne.asgi import GraphQL
from ariadne import make_executable_schema
from ariadne import load_schema_from_path

from starlette.middleware.cors import CORSMiddleware

from .routes.courses import query as courses_queries
from .routes.courses import mutation as courses_mutations
from .routes.companies import query as companies_queries, mutation as companies_mutations
from .routes.courses import query as course_queries
from .routes.inventory import query as inventory_queries
from .routes.inventory import mutation as inventory_mutations
from .routes.users import query as user_queries, mutation as user_mutations
from .routes.example import query as example_queries


type_defs = load_schema_from_path("./app/schemas/")


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')


schema = make_executable_schema(type_defs,courses_mutations,courses_queries, course_queries, companies_mutations, companies_queries, user_queries,
                                user_mutations, example_queries, inventory_mutations, inventory_queries)
app = CORSMiddleware(GraphQL(schema, debug=True), allow_origins=[
                     '*'], allow_methods=("GET", "POST", "OPTIONS"))
