import requests

from ariadne import QueryType
from graphql.type import GraphQLResolveInfo

from random import choice

from os import getenv
from dotenv import load_dotenv

load_dotenv()


query = QueryType()


@query.field("getRandomPlayer")
def resolve_get_player(obj, resolve_info: GraphQLResolveInfo):
    response = requests.get(f"{getenv('example_url')}/players")

    if response.status_code != 200:
        return
    
    id = choice(response.json())['id']

    response = requests.get(f"{getenv('example_url')}/players/{id}")

    if response.status_code == 200:
        return response.json()