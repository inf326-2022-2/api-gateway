import requests
from ariadne import QueryType,MutationType
from graphql.type import GraphQLResolveInfo

query = QueryType()
mutation = MutationType()


@query.field("getCourse")
def resolve_get_player(obj, resolve_info: GraphQLResolveInfo):
    return {"id": "INF326", "name": "Arquitectura de Software"}

@query.field("getProvider")
def resolve_get_provider(obj, resolve_info: GraphQLResolveInfo, id):
    response = requests.get(f"http://proveedores_service/proveedor/{id}")

    if response.status_code == 200:
        return response.json()


""" @team.field("players") """
@query.field("listProvider")
def resolve_list_players(obj, resolve_info: GraphQLResolveInfo):
    
    response = requests.get(f"http://proveedores_service/proveedor")
    print(response.json())
    if response.status_code == 200:
        return response.json()


""" @player.field("team") """
@query.field("listPurchase")
def resolve_list_purchase(obj, resolve_info: GraphQLResolveInfo):
    
    response = requests.get(f"http://proveedores_service/compras")
    if response.status_code == 200:
        return response.json()




@mutation.field("createProvider")
def resolve_create_provider(obj, resolve_info: GraphQLResolveInfo, name,  description=None):
    payload = dict(name=name
                   )

    if description:
        payload['description'] = description

    return requests.post(f"http://proveedores_service/proveedor", json=payload).json()
