from ariadne import QueryType, MutationType
from graphql.type import GraphQLResolveInfo
import requests

from os import getenv
from dotenv import load_dotenv

load_dotenv()

query = QueryType()

url = getenv('inventory_url')


@query.field("getInventory")
def resolve_get_all_products(obj, resolve_info: GraphQLResolveInfo):
    response = requests.get(f"{url}/inventory/all")
    if response.status_code == 200:
        response = response.json()
        products = response['products']
        return products
    else:
        return []


@query.field("getProduct")
def resolve_get_product(obj, resolve_info: GraphQLResolveInfo, _id):
    try:
        response = requests.get(f"{url}/inventory/{_id}")
        if response.status_code == 200:
            response = response.json()
            response["_id"] = response['id']
            response['proveedor'] = None
            return response
    except AttributeError:
        return {"msg": "error"}


mutation = MutationType()


@mutation.field("CreateProduct")
def resolve_create_product(obj, resolve_info: GraphQLResolveInfo, product):
    try:
        response = requests.post(
            f"{url}/inventory", json=product)
        if response.status_code == 200:
            return 'Success'
        else:
            return 'Error'
    except AttributeError:
        return 'Error'


@mutation.field("UpdateProduct")
def resolve_update_product(obj, resolve_info: GraphQLResolveInfo, product):
    try:
        response = requests.put(
            f"{url}/inventory/{product['_id']}", json=product)
        if response.status_code == 200:
            return 'Success'
        else:
            return 'Error'
    except AttributeError:
        return 'Error'


@mutation.field("DeleteProduct")
def resolve_delete_product(obj, resolve_info: GraphQLResolveInfo, _id):
    try:
        response = requests.delete(f"{url}/inventory/{_id}")
        if response.status_code == 200:
            return "Success"
        else:
            return 'Error'
    except AttributeError:
        return "Error"
