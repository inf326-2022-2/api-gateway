import logging

from ariadne import QueryType, MutationType
from graphql.type import GraphQLResolveInfo
from ..services.user import UserService

query = QueryType()
mutation = MutationType()

@query.field("login")
def resolver_login(_, info, username, password):
    login_result = UserService.login(username, password)
    return login_result

@query.field("getUsers")
def resolver_get_users(_, info):
    users = UserService.getUsers()
    return users

@query.field("getUser")
def resolver_get_user(_, info, id):
    user = UserService.getUser(id)
    return user

@mutation.field("deleteUser")
def resolver_delete_user(_, info, id):
    message = UserService.deleteUsers(id)
    return message

@mutation.field("updateUser")
def resolver_update_user(_, info, id, username=None, password=None):
    message = UserService.updateUser(id, username=username, password=password)
    return message

@mutation.field("createUser")
def resolver_create_user(_, info, username, password):
    new_user = UserService.createUser(username, password)
    return new_user

### Permissions ####

@query.field("getPermissions")
def resolver_get_permissions(_, info):
    permissions = UserService.getPermissions()
    return permissions

@query.field("getPermission")
def resolver_get_permission(_, info, id):
    permission = UserService.getPermission(id)
    return permission

@mutation.field("createPermission")
def resolver_create_permission(_, info, tag):
    new_permission = UserService.createPermission(tag)
    return new_permission

@mutation.field("updatePermission")
def resolver_update_permission(_, info, permission_id, new_tag):
    updated_permission = UserService.updatePermission(permission_id, new_tag)
    return updated_permission

@mutation.field("deletePermission")
def resolver_delete_permission(_, info, permission_id):
    deleted_permission = UserService.deletePermission(permission_id)
    return deleted_permission

@mutation.field("permissionAssing")
def resolver_permission_assing(_, info, permission_id, user_id):
    response = UserService.assingPermission(user_id, permission_id)
    return response

@query.field("getUserPermissions")
def resolver_get_user_permissions(_, info, user_id):
    response = UserService.getUserPermission(user_id)
    return response