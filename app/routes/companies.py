import time
import logging
import requests

from ariadne import QueryType
from ariadne import MutationType
from ariadne import ObjectType
from ariadne import make_executable_schema
from ariadne import load_schema_from_path

from ariadne.asgi import GraphQL

from graphql.type import GraphQLResolveInfo

from starlette.middleware.cors import CORSMiddleware


from os import getenv
from dotenv import load_dotenv

load_dotenv()



type_defs = load_schema_from_path("./app/schemas/")

query = QueryType()
mutation = MutationType()

pedido= ObjectType("Pedido")
empresa = ObjectType("Empresa")


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')


## ESTAS SON LAS QUERIES DE PEDIDOS

@query.field("getPedido")
def resolve_get_pedido(obj, resolve_info: GraphQLResolveInfo, pedido_id):
    response = requests.get(f"{getenv('companies-service')}/pedidos/{pedido_id}")

    if response.status_code == 200:
        return response.json()

@query.field("getPedidos")
def resolve_get_pedidos(obj, resolve_info: GraphQLResolveInfo):
    response = requests.get(f"{getenv('companies-service')}/pedidos")

    if response.status_code == 200:
        return response.json()

@mutation.field("createPedido")
def resolve_create_pedido(obj, resolve_info: GraphQLResolveInfo, id_empresa, fecha, costo):
    payload = dict(id_empresa= id_empresa,
                   fecha= fecha,
                   costo = costo)
    response= requests.post(f"{getenv('companies-service')}/pedidos", json=payload)
    return response.json()

@mutation.field("updatePedido")
def resolve_update_pedido(obj, resolve_info: GraphQLResolveInfo, pedido_id, id_empresa, fecha, costo):
    payload = dict(
                id=pedido_id,
                id_empresa= id_empresa,
                fecha= fecha,
                costo = costo)
    response= requests.put(f"{getenv('companies-service')}/pedidos/{pedido_id}", json=payload)
    if response.status_code == 200:
        return response.json()

@mutation.field("deletePedido")
def resolve_delete_pedido(obj, resolve_info: GraphQLResolveInfo, pedido_id):
    response= requests.delete(f"{getenv('companies-service')}/pedidos/{pedido_id}")
    if response.status_code == 200:
        return response.json()

## ESTAS SON LAS QUERIES DE EMPRESAS

@query.field("getEmpresa")
def resolve_get_empresa(obj, resolve_info: GraphQLResolveInfo, empresa_id):
    response = requests.get(f"{getenv('companies-service')}/empresas/{empresa_id}")

    if response.status_code == 200:
        return response.json()

@query.field("getEmpresas")
def resolve_get_empresas(obj, resolve_info: GraphQLResolveInfo):
    response = requests.get(f"{getenv('companies-service')}/empresas")

    if response.status_code == 200:
        return response.json()

@mutation.field("createEmpresa")
def resolve_create_empresa(obj, resolve_info: GraphQLResolveInfo, nombre_empresa, rut_empresa, rubro):
    payload = dict(nombre_empresa = nombre_empresa, rut_empresa = rut_empresa, rubro = rubro)
    response= requests.post(f"{getenv('companies-service')}/empresas", json=payload)
    return response.json()

@mutation.field("updateEmpresa")
def resolve_update_empresa(obj, resolve_info: GraphQLResolveInfo, empresa_id, nombre_empresa, rut_empresa, rubro):
    payload = dict(nombre_empresa = nombre_empresa, rut_empresa = rut_empresa, rubro = rubro)
    response= requests.put(f"{getenv('companies-service')}/empresas/{empresa_id}", json=payload)
    if response.status_code == 200:
        return response.json()

@mutation.field("deleteEmpresa")
def resolve_delete_empresa(obj, resolve_info: GraphQLResolveInfo, empresa_id):
    response= requests.delete(f"{getenv('companies-service')}/empresas/{empresa_id}")
    if response.status_code == 200:
        return response.json()


