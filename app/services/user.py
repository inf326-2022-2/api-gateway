import os
import requests


class UserServiceInterface:
    def __init__(self, baseURL):
        self.baseURL = baseURL

    def login(self, username, password):
        payload = {"username": username, "password": password}
        response = requests.post(f"{self.baseURL}/login", json=payload)
        return response.json()

    def getUsers(self):
        users = requests.get(f"{self.baseURL}/users").json()
        for user in users:
            del user['password']
        return users
    
    def deleteUsers(self, user_id):
        deleted_user = requests.delete(f"{self.baseURL}/users", json={ "id": user_id }).json()
        return deleted_user

    def getUser(self, user_id):
        user = requests.get(f"{self.baseURL}/users/{user_id}").json()
        return user

    def updateUser(self, id, username=None, password=None):
        payload = { "id": id }
        
        if username != None:
            payload['username'] = username
        if password != None:
            payload['password'] = payload

        updated_user = requests.put(f"{self.baseURL}/users", json=payload).json()

        return updated_user

    def createUser(self, username, password):
        payload = { "username": username, "password": password }
        new_user = requests.post(f"{self.baseURL}/users", json=payload).json()
        del new_user["password"]
        return new_user

    ### Permissions ###
    # TODO: separar en otro archivo (posiblemente)
    def getPermissions(self):
        permissions = requests.get(f"{self.baseURL}/permissions").json()
        return permissions

    def getPermission(self, permission_id):
        permission = requests.get(f"{self.baseURL}/permissions/{permission_id}").json()
        return permission

    def createPermission(self, tag):
        payload = { "tag": tag }
        new_permission = requests.post(f"{self.baseURL}/permissions", json=payload).json()
        return new_permission

    def updatePermission(self, permission_id, new_tag):
        payload = { "tag": new_tag , "id": permission_id }
        updated_permission = requests.put(f"{self.baseURL}/permissions", json=payload).json()
        return updated_permission

    def deletePermission(self, permission_id):
        payload = { "id": permission_id }
        deleted_permission = requests.delete(f"{self.baseURL}/permissions", json=payload).json()
        return deleted_permission

    #########
    
    def assingPermission(self, user_id, permission_id):
        payload = { "permission_id": permission_id, "user_id": user_id }
        response = requests.post(f"{self.baseURL}/permissions/assing", json=payload)
        return response

    def getUserPermission(self, user_id):
        user_permissions = requests.get(f"{self.baseURL}/users/permissions/{user_id}").json()
        return user_permissions

UserService = UserServiceInterface(f"{os.environ['USERS_URL']}")
